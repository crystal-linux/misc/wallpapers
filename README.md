# Wallpapers

The official Crystal Linux wallpapers.


## 📸 Screenshots

![Wallpaper Light](https://cdn.discordapp.com/attachments/842491569176051712/1086985131002167407/Screenshot_from_2023-03-19_13-09-29.png)
![Wallpaper Dark](https://cdn.discordapp.com/attachments/842491569176051712/1086985130788278323/Screenshot_from_2023-03-19_13-09-37.png)

(Special thanks to @axtlos for the screenshots)

## 🙌 Contributing

If you'd like to contribute to the **Wallpapers**, please follow the [Crystal Linux contributing guidelines](https://git.getcryst.al/crystal/info/-/blob/main/CONTRIBUTING.md)!

## 📜 License

[GPLv3-only](https://choosealicense.com/licenses/gpl-3.0/)

![](https://git.getcryst.al/crystal/misc/branding/-/raw/main/banners/README-banner.png)
